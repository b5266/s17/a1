let names = [];


function addStudents(name){
	let namesLength = names.push(name);
	console.log(name + " was added to the student's list.");
}

function countStudents(){
	console.log("There are a total of " + names.length + " students enrolled: ");
}

function printStudents(){
	let namesSort = names.sort();
	names.forEach(function(name){
		console.log(name);
	})	
}

function findStudent(name){
	let filteredNames = names.filter(function(student){
		return student.toLowerCase().includes(name.toLowerCase());
	})
	if(filteredNames.length >= 2){
		console.log(filteredNames.toString() + ' are enrolles.')
	}
	else if(filteredNames.length == 1){
		console.log(name + ' is an enrollee.');
	}
	else{
		console.log('No student found with the name ' + name + '.');
	}
}